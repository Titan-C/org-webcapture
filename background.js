chrome.runtime.onInstalled.addListener(({ reason, previousVersion }) => {
  if (
    reason == "install" ||
    (reason == "update" && previousVersion.startsWith("0.1"))
  ) {
    chrome.storage.local.set({
      debug: false,
      overlay: true,
    });
  }
});

chrome.browserAction.onClicked.addListener((tab) => {
  browser.tabs.executeScript({ file: "capture.js" });
});
