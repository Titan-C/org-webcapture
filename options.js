// Saves options to chrome.storage.local.
function saveOptions() {
  chrome.storage.local.set(
    {
      webTemplate: document.getElementById("webTemplate").value,
      roam: document.getElementById("roam").checked,
      meetupTemplate: document.getElementById("meetupTemplate").value,
      meetupAPIKey: document.getElementById("meetupAPIKey").value,
      debug: document.getElementById("debug").checked,
      overlay: document.getElementById("overlay").checked,
    },
    () => {
      // Update status to let user know options were saved.
      let status = document.getElementById("status");
      status.textContent = "Options saved.";
      setTimeout(() => {
        status.textContent = "";
      }, 750);
    },
  );
}

// Restores select box and checkbox state using the preferences
// stored in chrome.storage.
function restoreOptions() {
  // Use default value color = 'red' and likesColor = true.
  chrome.storage.local.get(
    {
      webTemplate: "l",
      roam: true,
      meetupTemplate: "m",
      meetupAPIKey: "",
      debug: false,
      overlay: true,
    },
    (options) => {
      document.getElementById("webTemplate").value = options.webTemplate;
      document.getElementById("roam").checked = options.roam;
      document.getElementById("meetupTemplate").value = options.meetupTemplate;
      document.getElementById("meetupAPIKey").value = options.meetupAPIKey;
      document.getElementById("debug").checked = options.debug;
      document.getElementById("overlay").checked = options.overlay;
    },
  );
}
document.addEventListener("DOMContentLoaded", restoreOptions);
document.getElementById("save").addEventListener("click", saveOptions);
