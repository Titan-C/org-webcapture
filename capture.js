function escapeIt(text) {
  return encodeURIComponent(text)
    .replace(/\(/g, "%28")
    .replace(/\)/g, "%29")
    .replace(/'/g, "%27");
}
// escapeIt("h'?pu(nilas p p )") === "h%27%3Fpu%28nilas%20p%20p%20%29";

function cleanDescription(body) {
  return body.replace(/<\/?p>|<br ?\/?>/g, "\n");
}

function orgLinks(body) {
  return body.replace(/<a href="(.*?)".*?>(.*?)<\/a>/g, "[[$1][$2]]");
}

function padzero(num) {
  return num.toString().padStart(2, "0");
}

function orgdate(event_day, event_time, duration) {
  let weekday = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
  let date = new Date(event_day + " " + event_time);
  let date_org = date.toISOString().split("T")[0];
  let end_time = new Date(date.getTime() + duration);
  end_time = `${padzero(end_time.getHours())}:${padzero(
    end_time.getMinutes(),
  )}`;
  return `<${date_org} ${weekday[date.getDay()]} ${event_time}-${end_time}>\n`;
}

function orglocation({ venue: { name, address_1 } }) {
  return ":LOCATION: " + name + ", " + address_1 + "\n";
}
function captureURL({ roam, ...params }) {
  let emacsFunction = roam ? "roam-ref" : "capture";
  if (roam) params = { ...params, ref: params.url };
  let urlParams = new URLSearchParams(params);
  return `org-protocol://${emacsFunction}?${urlParams.toString()}`;
}

function getmeetupAPIurl(url, meetupAPIKey) {
  let meetup = new URL(url);

  if (meetup.hostname.endsWith("meetup.com")) {
    meetup.hostname = "api.meetup.com";
    if (meetupAPIKey) meetup.searchParams.set("key", meetupAPIKey);
    return meetup.toString();
  }

  return false;
}

function toggleOverlay() {
  var captureOverlay = "org-capture-capture-overlay";
  if (!document.getElementById(captureOverlay)) {
    var inner_div = document.createElement("div");
    inner_div.id = captureOverlay;
    inner_div.textContent = "Captured";

    document.body.appendChild(inner_div);

    var css = document.createElement("style");
    css.type = "text/css";
    css.textContent = `#org-capture-capture-overlay{
background-color: rgba(0,0,0,0.3); /* Black background with opacity */
display: none;
color: white;
font-size: 5em;
text-align: center;
padding: 1em 0;
position:absolute;
top:0;
width:100%;
height:100%;
z-index:1000;
}`;
    document.body.appendChild(css);
  }
  let overlay = document.getElementById(captureOverlay);
  overlay.style.display = "block";
  setTimeout(() => (overlay.style.display = "none"), 300);
}

function webTemplate({ webTemplate, roam }) {
  return captureURL({
    template: webTemplate,
    title: document.title,
    body: window.getSelection().toString(),
    url: location.href,
    roam,
  });
}
function meetupBody(result) {
  let property = ":PROPERTIES:\n" + orglocation(result) + ":END:\n";
  let date = orgdate(result.local_date, result.local_time, result.duration);

  let howtofind = result.how_to_find_us;
  let link =
    (howtofind ? "\n" + howtofind + "\n" : "") + "\n" + result.link + "\n\n";
  let description = cleanDescription(orgLinks(result.description));
  return property + date + link + description;
}
function meetupTemplate(meetupAPIurl, { meetupTemplate }) {
  return fetch(meetupAPIurl)
    .then((response) => response.json())
    .then((result) => {
      console.log(result);
      let body = meetupBody(result);
      return captureURL({
        template: meetupTemplate,
        title: result.name,
        url: result.link,
        body,
        roam: false,
      });
    });
}

function callCapture(uri, options) {
  if (options.debug) console.log("Capturing URI | ", uri);

  location.href = uri;

  if (options.overlay) toggleOverlay();
}

function capture(options) {
  if (chrome.runtime.lastError) {
    alert(
      "Could not capture url. Error loading options: " +
        chrome.runtime.lastError.message,
    );
  }

  let meetupAPIurl = getmeetupAPIurl(location.href, options.meetupAPIKey);
  if (meetupAPIurl) {
    meetupTemplate(meetupAPIurl, options).then((url) =>
      callCapture(url, options),
    );
  } else {
    callCapture(webTemplate(options), options);
  }
}

chrome.storage.local.get(
  ["webTemplate", "roam", "meetupTemplate", "meetupAPIKey", "debug", "overlay"],
  capture,
);
